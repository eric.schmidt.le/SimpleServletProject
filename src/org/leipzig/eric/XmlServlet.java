package org.leipzig.eric;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class XmlServlet extends HttpServlet {

    ///SimpleServletProject/xmlServletPath?userName=Eric
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String calcOp = request.getParameter("calc");
        String vala= request.getParameter("val1");
        String valb= request.getParameter("val2");
        int re = 0;

        switch (calcOp) {
            case "Addition":  re=doAddition(vala,valb );
                break;
            case "Subtraction":  re=doSubtraction(vala,valb );
                break;
            case "Multiplication":  re=doMultiplication(vala,valb );
                break;
            case "Division": re=doDivision(vala,valb );
                break;
            default: re = 0;
                break;
        }

        response.setContentType("text/html");

        PrintWriter out = response.getWriter();
        out.println(re);
    }

    private int doAddition(String vala, String valb){
        return Integer.parseInt(vala)+Integer.parseInt(valb);
    }
    
    private int doSubtraction(String vala, String valb){
        return Integer.parseInt(vala)-Integer.parseInt(valb);
    }
    
    private int doMultiplication(String vala, String valb){
        return Integer.parseInt(vala)*Integer.parseInt(valb);
    }
    
    private int doDivision(String vala, String valb){
        return Integer.parseInt(vala)/Integer.parseInt(valb);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        System.out.println("XML servlet called!");

        response.setContentType("text/html");
        String userName = request.getParameter("userName");
        PrintWriter out = response.getWriter();
        out.println("Hello from POST method" + userName);
    }

}
